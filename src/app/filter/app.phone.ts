import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'phone'
})

export class PhonePipe implements PipeTransform {
    transform(val, args) {
        return '(' + val.substr(0, 3) + ') ' + val.substr(4, 3) + val.substr(7, 5);
    }
}
