import { Pipe, PipeTransform } from '@angular/core';

import { Contact } from '../model/contact';

@Pipe({
    name: 'favoritefilter',
    pure: false
})

export class FavoriteFilterPipe implements PipeTransform {
    transform(items: Contact[], filter: string): any {
        if (!items || !filter) {
            return items;
        }

        return items.filter(item => item.isFavorite === JSON.parse(filter));
    }
}
