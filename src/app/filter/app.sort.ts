import { Pipe, PipeTransform } from '@angular/core';

import { Contact } from '../model/contact';

@Pipe({
    name: 'sort'
})

export class SortFilterPipe implements PipeTransform {
    transform(items: Contact[], field: string): any[] {
        items.sort((a: any, b: any) => {
            if (a[field] < b[field]) {
                return -1;
            } else if (a[field] > b[field]) {
                return 1;
            } else {
                return 0;
            }
        });

        return items;
    }
}
