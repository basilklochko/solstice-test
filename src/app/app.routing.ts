import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppListComponent } from './component/app.list';
import { AppDetailsComponent } from './component/app.details';

export const AppPaths: Routes = [
    { path: '', redirectTo: 'list', pathMatch: 'full' },
    { path: 'list', component: AppListComponent },
    { path: 'details/:id', component: AppDetailsComponent },
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(AppPaths);
