import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Contact } from '../model/contact';

@Injectable()
export class ContactService {
    private url = 'https://s3.amazonaws.com/technical-challenge/v3/contacts.json';

    public contacts: Contact[] = [];

    constructor(private http: Http) {

    }

    public GetContacts(): Observable<Contact[]> {
        return this.http.get(this.url)
            .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    public GetContactById(id: string): Contact {
        const res = this.contacts.filter(contact => contact.id === id);

        if (res.length > 0) {
            return res[0] as Contact;
        } else {
            return new Contact();
        }
    }
}
