import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppPaths, AppRouting } from './app.routing';

import { ContactService } from './service/contactService';

import { FavoriteFilterPipe } from './filter/app.favorite';
import { SortFilterPipe } from './filter/app.sort';
import { PhonePipe } from './filter/app.phone';

import { AppMainComponent } from './component/app.main';
import { AppListComponent } from './component/app.list';
import { AppDetailsComponent } from './component/app.details';

import { AppContactLineComponent } from './component/app.contactline';
import { AppPhoneLineComponent } from './component/app.phoneline';

@NgModule({
  declarations: [
    AppMainComponent,
    AppListComponent,
    AppDetailsComponent,

    AppContactLineComponent,
    AppPhoneLineComponent,

    FavoriteFilterPipe,
    SortFilterPipe,
    PhonePipe
  ],
  imports: [
    BrowserModule, HttpModule, AppRouting
  ],
  providers: [ContactService],
  bootstrap: [AppMainComponent]
})

export class AppModule { }
