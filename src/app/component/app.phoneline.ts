import { Component, Input, OnInit } from '@angular/core';

import { PhoneEnum } from '../enum/phoneEnum';

@Component({
    selector: 'app-phone-line',
    templateUrl: '../template/app.phone-line.html'
})

export class AppPhoneLineComponent implements OnInit {
    @Input()
    type: PhoneEnum;

    @Input()
    number: string;

    public displayType: any;

    ngOnInit(): void {
        this.displayType = PhoneEnum[this.type];
    }
}
