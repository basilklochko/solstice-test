import { Component, Input } from '@angular/core';

import { Contact } from '../model/contact';

@Component({
    selector: 'app-contact-line',
    templateUrl: '../template/app.contact-line.html'
})

export class AppContactLineComponent {
    @Input()
    contact: Contact;
}
