import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ContactService } from '../service/contactService';

import { Contact } from '../model/contact';

@Component({
  selector: 'app-list',
  templateUrl: '../template/app.list.html'
})

export class AppListComponent implements OnInit {
  public contacts: Contact[] = [];

  constructor(private contactService: ContactService, private router: Router) {

  }

  ngOnInit(): void {
    if (this.contactService.contacts.length === 0) {
      this.contactService.GetContacts().subscribe(res => {
        this.onGetContacts(res);
      });
    } else {
      this.onGetContacts(this.contactService.contacts);
    }
  }

  public onContactClick(id: string) {
    this.router.navigate(['/details/' + id]);
  }

  private onGetContacts(res: any): void {
    if (res != null) {
      this.contacts = res as Contact[];

      this.contactService.contacts = this.contacts;
    }
  }
}
