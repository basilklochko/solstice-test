import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ContactService } from '../service/contactService';

import { PhoneEnum } from '../enum/phoneEnum';
import { Contact } from '../model/contact';

@Component({
  selector: 'app-details',
  templateUrl: '../template/app.details.html'
})

export class AppDetailsComponent implements OnInit {
  public contact: Contact = new Contact();
  private sub: any;

  constructor(private contactService: ContactService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    const self = this;

    self.sub = self.route.params.subscribe(params => {
      if (params['id'] !== undefined && params['id'] != null) {
        const id = params['id'];

        if (self.contactService.contacts.length === 0) {
          self.contactService.GetContacts().subscribe(res => {
            self.contactService.contacts = res as Contact[];

            self.contact = self.contactService.GetContactById(id);
          });
        } else {
          self.contact = self.contactService.GetContactById(id);
        }
      }
    });
  }

  public onFavorite() {
    this.contact.isFavorite = !this.contact.isFavorite;
  }
}
