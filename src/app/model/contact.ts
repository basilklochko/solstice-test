export class Contact {
    public id: string;
    public name: string;
    public companyName: string;
    public isFavorite: boolean;
    public phone: Phone;
    public address: Address;
    public birthdate: string;
    public emailAddress: string;
    public smallImageURL: string;
    public largeImageURL: string;

    constructor() {
        this.phone = new Phone();
        this.address = new Address();
    }
}

export class Phone {
    public work: string;
    public home: string;
    public mobile: string;
}

export class Address {
    public street: string;
    public city: string;
    public state: string;
    public country: string;
    public zipCode: string;
}
