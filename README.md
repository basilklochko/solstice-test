## Solstice Code Challenge ##

# Steps: #

* Clone locally 
# git clone https://bitbucket.org/basilklochko/solstice #


* Change folder to the project's one
# cd solstice #

* Install npm modules
# npm install #

* Run cli runtime
# ng serve #

* In browser navigate to 
# http://localhost:4200/ #